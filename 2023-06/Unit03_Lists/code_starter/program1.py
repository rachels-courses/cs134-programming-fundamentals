planets = [
  "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus",
  "Neptune", "Pluto"
]

print("The planets are...")
for planet in planets:
  print(planet)

print()
print("Oops, actually, it's...")

# Student task:
# Remove Pluto from the list with either the del command
# or the pop command then display the updated list again
# using one of the two forms of for loops.
