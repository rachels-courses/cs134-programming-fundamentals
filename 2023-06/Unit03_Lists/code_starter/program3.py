import random  # Allow us to use random numbers

print("NUMBER LIST")

numbers = [
  random.randint(1, 100),  # Generate a random number between [1,100]
  random.randint(1, 100),  # Generate a random number between [1,100]
  random.randint(1, 100),  # Generate a random number between [1,100]
  random.randint(1, 100),  # Generate a random number between [1,100]
]

# Display list of numbers
print()
print("RANDOM NUMBER LIST:")
print("\t Index \t Number")
for i in range(len(numbers)):
  print("\t", i, "\t", numbers[i])

# 1. Ask user to enter indices for two of these numbers
print()
print("We are going to add two numbers together!")

# 2. Calculate the sum of the two numbers together

# 3. Display the result
print()
