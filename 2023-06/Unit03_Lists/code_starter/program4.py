# --------------------------------------------------------------------- #
# - Creating Data -
# --------------------------------------------------------------------- #
course_codes = [
  "CS134", "CS200", "CS201", "CS202", "CS205", "CS210", "CS211", "CS235",
  "CS236", "CS250", "CS252", "CS255"
]

course_names = [
  "Programming Fundamentals",
  "Concepts of Programming Algorithms Using C++",
  "Concepts of Programming Algorithms using C#",
  "Concepts of Programming Algorithms using Python",
  "Concepts of Programming Algorithms using Java",
  "Discrete Structures I",
  "Discrete Structures II",
  "Object-Oriented Programming Using C++",
  "Object-Oriented Programming Using C#",
  "Basic Data Structures using C++",
  "Basic Data Structures Using Python",
  "Basic Data Structures Using Java",
]

course_descriptions = [
  "In this introductory course, students will create interactive computer applications that perform tasks and solve problems. Students will design, develop and test object-oriented programs that utilize fundamental logic, problem-solving techniques and key programming concepts. 3 hrs. lecture, 2 hrs. open lab /wk.",
  "This course emphasizes problem solving using a high-level programming language and the software development process. Algorithm design and development, programming style, documentation, testing and debugging will be presented. Standard algorithms and data structures will be introduced. Data abstraction and an introduction to object-oriented programming will be studied and used to implement algorithms. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.",
  "This course emphasizes problem-solving using a high-level programming language and the software development process. Algorithm design and development, programming style, documentation, testing and debugging will be presented. Standard algorithms and data structures will be introduced. Data abstraction and an introduction to object-oriented programming will be studied and used to implement algorithms.",
  "This course emphasizes problem-solving using a high-level programming language and the software development process. Algorithm design and development, classes and inheritance, programming style, documentation, testing and debugging will be presented. Standard algorithms and data structures will be introduced. Data abstraction and object-oriented programming will be studied and used to implement algorithms. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.",
  "This course emphasizes problem-solving using a high-level programming language and the object-oriented software development process. Algorithm design and development, classes and inheritance, programming style, documentation, testing and debugging will be presented. Standard algorithms and data structures will be introduced. Data abstraction and object-oriented programming will be studied and used to implement algorithms. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.",
  "This course focuses on the study of topics in Discrete Structures aimed at applications in Computer Science. Students will study logic, methods of proof including induction, set theory, relations, functions and Boolean algebra. They will develop general problem-solving skills.",
  "This second course in Discrete Structures is aimed at solving problems in Computer Science. Students will study computation, induction, recursion, integers, counting, graphs and trees. They will develop general problem-solving and programming skills.",
  "This course emphasizes programming methodology and problem solving using the object-oriented paradigm. Students will develop software applications using the object-oriented concepts of data abstraction, encapsulation, inheritance, and polymorphism. Students will apply the C++ techniques of dynamic memory, pointers, built-in classes, function and operator overloading, exception handling, recursion and templates. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.",
  "This course prepares students to develop object-oriented, C# applications that solve a variety of problems. Students will apply object-oriented concepts including inheritance, function overloading and polymorphism, and will utilize available classes as well as design their own. Event-driven programming, Windows applications, web development, common data structures, database access and frameworks will be presented. 3 hrs. lecture, 2 hrs. open lab/wk.",
  "This course continues developing problem solving techniques by focusing on object-oriented styles using C++ abstract data types. Basic data structures such as queues, stacks, trees, dictionaries, their associated operations, and their array and pointer implementations will be studied. Topics also include recursion, templates, fundamental algorithm analysis, searching, sorting, hashing, object-oriented concepts and large program organization. Students will write programs using the concepts covered in the lecture. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.",
  "This course continues developing advanced problem-solving techniques through object-oriented programming using Python. Basic data structures including stacks, queues, trees, and dictionaries will be studied and implemented. Additional topics include recursion, fundamental algorithm analysis, searching, sorting, and large program organization. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.",
  "This course will cover advanced programming topics using Java. Files, recursion, data structures and large program organization will be implemented in projects using object-oriented methodology. Students will write programs using queues, stacks, lists and other concepts covered in the lecture. 3 hrs. lecture, 2 hrs. lab by arrangement/wk."
]

# --------------------------------------------------------------------- #
# - Program code -
# --------------------------------------------------------------------- #

print("COURSE CATALOG")
print("Select a course by its number code:")

# 1. Traverse the list of course codes, displaying each index and course code

print()
# 2. Ask the user which class they want to see, get the input as an int.

print()

# 3. Display information from course_codes, course_names, and course_description
#     all using the index entered by the user.
