# Run this program with:
# python3 program1_example.py

print( "Sugar cookie recipe" )
print( "1.5 cups powdered sugar" )
print( "1 cup butter" )
print( "1 teaspoon vanilla" )
print( "1 egg" )
print( "1 teaspoon baking soda" )
print( "2.5 cups all-purpose flour" )
