# PRACTICE: PRINTING (NO VARIABLES)
#
# Using the print statement, write a program that
# displays the today's day and the next 2 days
# and the projected temperature for each day.
#
# Look up the weather forecast and hard-code the temperatures,
# this assignment won't use any variables.
#
# EXAMPLE OUTPUT (use temperatures from a weather site):
# Sunday 84 F
# Monday 74 F
# Tuesday 64 F 
#
# TO RUN THE PROGRAM, TYPE THIS IN THE SHELL (without #):
# python3 program1_student1.py

# Add code beneath this line
