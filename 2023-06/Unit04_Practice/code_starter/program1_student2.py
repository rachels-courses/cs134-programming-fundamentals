# PRACTICE: PRINTING (NO VARIABLES)
#
# Using the print statement, write a program that
# displays a list of 5 product names and prices.
#
# Look up some products off a store website
# and hard-code the product names and prices.
# This assignment won't use any variables.
#
# EXAMPLE OUTPUT (use names/prices from a store website):
# Lemons $0.88
# Roma Tomatoes $0.45
# Cookies $3.50
# Bananas $0.40
# Kiwi Fruit $0.73
#
# TO RUN THE PROGRAM, TYPE THIS IN THE SHELL (without #):
# python3 program1_student2.py

# Add code beneath this line
