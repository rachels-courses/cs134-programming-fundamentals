# PRACTICE: PRINTING (NO VARIABLES)
#
# Using the print statement, write a program that
# displays a list of 3 course codes and titles.
# https://catalog.jccc.edu/coursedescriptions/
#
# Look up some products off a store website
# and hard-code the course codes and titles.
# This assignment won't use any variables.
#
# EXAMPLE OUTPUT (use codes/titles from a college catalog):
# ART 124 Design 2D
# DHYG 121 Clinical Dental Hygiene I
# CS 134 Programming Fundamentals
#
# TO RUN THE PROGRAM, TYPE THIS IN THE SHELL (without #):
# python3 program1_student3.py

# Add code beneath this line
