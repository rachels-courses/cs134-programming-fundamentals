# Run this program with:
# python3 program2_example.py

# Variable creations:
ing1_name = "cups powdered sugar"
ing2_name = "cups butter"
ing3_name = "teaspoons vanilla"
ing4_name = "eggs"
ing5_name = "teaspoons baking soda"
ing6_name = "cups all-purpose flour"

ing1_amount = 1.5
ing2_amount = 1
ing3_amount = 1
ing4_amount = 1
ing5_amount = 1
ing6_amount = 2.5

# Printing the recipe with the variables:
print( "Sugar cookie recipe" )

print( ing1_amount, ing1_name )
print( ing2_amount, ing2_name )
print( ing3_amount, ing3_name )
print( ing4_amount, ing4_name )
print( ing5_amount, ing5_name )
print( ing6_amount, ing6_name )
