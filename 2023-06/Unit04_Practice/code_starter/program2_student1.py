# PRACTICE: PRINTING (WITH VARIABLES)
#
# MODIFY your program1 to now use variables.
# Create the following variables:
# day1_name
# day2_name
# day3_name
#
# day1_temp
# day2_temp
# day3_temp
#
# Store string values like "Monday", "Tuesday", etc. in the day name variables.
# Store float values like 74.0, 64.0, etc. in the day temp variables.
#
# Modify your print statements to display the day name and temp
# from the variables. 
#
# TO RUN THE PROGRAM, TYPE THIS IN THE SHELL (without #):
# python3 program2_student1.py

# Add code beneath this line
