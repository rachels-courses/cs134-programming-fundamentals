# PRACTICE: PRINTING (WITH VARIABLES)
#
# MODIFY your program1 to now use variables.
# Create the following variables:
# product1_name
# product2_name
# product3_name
#
# product1_price
# product2_price
# product3_price
#
# Store strings like "Lemons" in the product name variables.
# Store floats like 0.88 in the product price variables.
#
# Modify your print statements to display the product name and price
# from the variables.
#
# TO RUN THE PROGRAM, TYPE THIS IN THE SHELL (without #):
# python3 program2_student2.py

# Add code beneath this line
