# PRACTICE: PRINTING (WITH VARIABLES)
# 
# MODIFY your program1 to now use variables.
# Create the following variables:
# course1_code
# course2_code
# course3_code
#
# course1_title
# course2_title
# course3_title
#
# The course code variables should store string values like "ART 124"
# and the course title variables should store string values like "Design 2D".
#
# Update your print statements to display the course code and course titles.
#
# TO RUN THE PROGRAM, TYPE THIS IN THE SHELL (without #):
# python3 program2_student3.py

# Add code beneath this line
