# Run this program with:
# python3 program3_example.py

# List creations:

ing_names = [ "cups powdered sugar", "cups butter", "teaspoons vanilla", "eggs", "teaspoons baking soda", "cups all-purpose flour" ]
ing_amounts = [ 1.5, 1, 1, 1, 1, 2.5 ]

# Printing the recipe with the variables:
print( "Sugar cookie recipe" )

for x in range( len( ing_names ) ):
  print( ing_amounts[x], ing_names[x] )
