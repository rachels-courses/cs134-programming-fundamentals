# PRACTICE: PRINTING (WITH LISTS)
# 
# MODIFY your program1 to now use lists.
# 
# Create two lists:
# day_names
# day_temps
#
# Fill these two lists with the values from v2 of the program,
# which you original stored in day1_name, day1_temp, etc.
#
# Update the print statement portion of the program to use a for loop
# to iterate over all elements of the list. See the program3_example.
#
# TO RUN THE PROGRAM, TYPE THIS IN THE SHELL (without #):
# python3 program3_student1.py

# Add code beneath this line
