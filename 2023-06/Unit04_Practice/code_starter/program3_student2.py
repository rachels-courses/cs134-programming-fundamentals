# PRACTICE: PRINTING (WITH LISTS)
# 
# MODIFY your program1 to now use lists.
#
# Create two lists:
# product_names
# product_prices
#
# Fill these two lists with the values from v2 of the program,
# which you original stored in product1_name, product1_price, etc.
#
# Update the print statement portion of the program to use a for loop
# to iterate over all elements of the list. See the program3_example.
#
# TO RUN THE PROGRAM, TYPE THIS IN THE SHELL (without #):
# python3 program3_student2.py

# Add code beneath this line
