# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentA_p1_if.py
# 
# 1. Ask the user "How many points possible?"
#    and store it in a variable, `total_points`.
# 2. Ask the user "How many points earned?"
#    and store it in a variable, `earned_points`.
# 3. Calculate the result percent with:
#    `earned_points / total_points * 100`
#    Store the result in a variable, `score_percent`.
# 4. Print out the score % to the screen.
# 5. If the `score_percent` is equal to 100, then
#    5a. Print the text "PERFECT SCORE!"
# 6. Display "Goodbye" at the end of the program
# 
# EXAMPLE OUTPUT - Not 100%:
# GRADE CALCULATOR
# How many points possible? 50
# How many points earned? 45
# Score: 90.0 %
# Goodbye
# 
# EXAMPLE OUTPUT - 100%:
# GRADE CALCULATOR
# How many points possible? 40
# How many points earned? 40
# Score: 100.0 %
# PERFECT SCORE!
# Goodbye

print( "GRADE CALCULATOR" )



print( "Goodbye" )
