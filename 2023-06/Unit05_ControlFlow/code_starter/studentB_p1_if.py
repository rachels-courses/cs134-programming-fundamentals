# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentB_p1_if.py
# 
# 1. Ask the user "What is the bank balance?"
#    and store it in a variable, `balance`.
# 2. Ask the user "How much are you withdrawing?"
#    and store it in a variable, `withdraw`.
# 3. Calculate the remaining balance with:
#    `balance - withdraw`
#    Store the result in a variable, `remaining`.
# 4. Print out the remaining balance to the screen.
#    You can format it as currency like:
#    "${:,.2f}".format( remaining )
# 5. If the `remaining` is less than 0, then
#    5a. Print the text "OVERDRAWN!"
# 6. Display "Goodbye" at the end of the program
# 
# EXAMPLE OUTPUT - Not overdrawn:
# BANK BALANCE
# What is the bank balance? $100
# How much are you withdrawing? $90
# Remaining balance: $10.00
# Goodbye
# 
# EXAMPLE OUTPUT - Overdrawn:
# BANK BALANCE
# What is the bank balance? $50
# How much are you withdrawing? $100
# Remaining balance: $-50.00
# OVERDRAWN!
# Goodbye

print( "BANK BALANCE" )



print( "Goodbye" )
