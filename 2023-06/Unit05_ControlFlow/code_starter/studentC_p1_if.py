# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentC_p1_if.py
# 
# 1. Ask the user "How many gallons of gas can be stored?"
#    and store it in a variable, `total_capacity`.
# 2. Ask the user "How many gallons of gas are there currently?"
#    and store it in a variable, `current_capacity`.
# 3. Calculate the result percent with:
#    `current_capacity / total_capacity * 100`
#    Store the result in a variable, `remaining_percent`.
# 4. Print out the `remaining_percent` to the screen.
# 5. If the `remaining_percent` is less than or equal to 10, then
#    5a. Print the text "LOW FUEL!"
# 6. Display "Goodbye" at the end of the program
# 
# EXAMPLE OUTPUT - Not low:
# GAS REMAINING
# How many gallons of gas can be stored? 12
# How many gallons of gas are there currently? 10
# Remaining gas: 83.33333333333334 %
# Goodbye
# 
# EXAMPLE OUTPUT - Low gas:
# GAS REMAINING
# How many gallons of gas can be stored? 12
# How many gallons of gas are there currently? 1
# Remaining gas: 8.333333333333332 %
# LOW FUEL!
# Goodbye

print( "GAS REMAINING" )



print( "Goodbye" )
