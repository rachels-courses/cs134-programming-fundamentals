# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentC_p5_while.py
# 
# 1. Ask the user to enter a low number and store it in variable `low`.
# 2. Ask the user to enter a high number and store it in variable `high`.
# 3. Create a `counter` variable and assign it the value of `low`.
# 4. Create a while loop: while `counter` is less than or equal to `high`, then:
#    4a. Print out `counter`'s value.
#    4b. Calculate `counter * 2` and store it back in the `counter` variable.
# 5. Display "Goodbye" at the end

print( "COUNTER" )



print( "Goodbye" )
