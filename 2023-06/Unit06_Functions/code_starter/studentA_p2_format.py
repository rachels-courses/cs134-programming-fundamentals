# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentA_p2_format.py
#
# FUNCTION 1: FormatUSD   Input: `money`, a float
#   Within the function, print the `money` value, formatted as USD.
#   print( "${:,.2f}".format( money ) )
#
# PROGRAM:
# 1. Display program name "FORMATTING PROGRAM".
# 2. Ask the user to enter a price, get their float input and store it in a variable `price`.
# 3. Call the FormatUSD function, passing in the `price`.
# 4. At the end of the program, print "Goodbye"


## -- FUNCTION DEFINITIONS --



## -- PROGRAM MAIN --

