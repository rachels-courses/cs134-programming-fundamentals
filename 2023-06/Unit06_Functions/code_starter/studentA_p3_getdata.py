# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentA_p3_getdata.py
#
# FUNCTION 1: GetTaxPercent   No inputs
#   Within the function, return the value `9.61`
#
# PROGRAM:
# 1. Display program name "GET DATA PROGRAM".
# 2. Call `GetTaxPercent()`, storing its result in a variable, `tax`.
# 3. Display "The tax percent is:", then display the value of ` tax`.
# 4. At the end of the program, print "Goodbye"


## -- FUNCTION DEFINITIONS --



## -- PROGRAM MAIN --

