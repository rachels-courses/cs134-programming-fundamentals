# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentA_p4_inout.py
#
# FUNCTION 1: GetAverage    Inputs: `num1`, `num2`, `num3`.
#   Within the function, return `( num1 + num2 + num3 ) / 3`
#
# PROGRAM:
# 1. Display program name "FUNCTION IN/OUT PROGRAM".
# 2. Ask the user to enter a first number. Store it in a float variable `number1`.
# 3. Ask the user to enter a second number. Store it in a float variable `number2`.
# 4. Ask the user to enter a third number. Store it in a float variable `number3`.
# 5. Call the function `GetAverage`, passing in `number1`, `number2`, and `number3`. Store the result in a variable `average`.
# 6. Display "The average is:", and then the `average` variable.
# 7. At the end of the program, print "Goodbye"


## -- FUNCTION DEFINITIONS --



## -- PROGRAM MAIN --

