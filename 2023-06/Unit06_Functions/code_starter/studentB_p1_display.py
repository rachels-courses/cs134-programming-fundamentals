# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentA_p1_display.py
#
# FUNCTION 1: RecommendMovie  no inputs
#   Print "Recommended movie:" and then a movie you like.
#
# FUNCTION 2: RecommendBook   no inputs
#   Print "Recommended book:" and then a book you like.
#
# FUNCTION 3: RecommendTVShow no inputs
#   Print "Recommended TV show:" and then a tv show you like.
#
# PROGRAM:
# 1. Display "RECOMMENDATION PROGRAM"
# 2. Display a menu, 1 to get a movie, 2 to get a book, and 3 to get a tv show.
# 3. Get the user's choice as an integer input, store it in a variable `choice`.
# 4a. If choice is 1, call the RecommendMovie() function.
# 4b. Else, if choice is 2, call the RecommendBook() function.
# 4c. Else, if choice is 3, call the RecommendTVShow() function.
# 5. After the if statements, print "Goodbye" at the end of the program.

## -- FUNCTION DEFINITIONS --



## -- PROGRAM MAIN --

