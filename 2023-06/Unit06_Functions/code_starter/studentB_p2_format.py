# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentB_p2_format.py
#
# FUNCTION 1: FormatName   Input: two strings, `firstname` and `lastname`.
#   Within the function, print the names in "last, first" order:
#   print( lastname + ", " + firstname )
#
# PROGRAM:
# 1. Display program name "FORMATTING PROGRAM".
# 2. Ask the user to enter a first name. Store it in a variable `name1`.
# 3. Ask the user to enter a last name. Store it in a variable `name2`.
# 4. Call the FormatName function, passing in the `name1` and `name2`.
# 5. At the end of the program, print "Goodbye"


## -- FUNCTION DEFINITIONS --



## -- PROGRAM MAIN --

