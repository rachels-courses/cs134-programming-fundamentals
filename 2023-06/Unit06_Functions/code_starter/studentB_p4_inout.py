# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentB_p4_inout.py
#
# FUNCTION 1: GetSquare    Inputs: `num`
#   Within the function, return `num * num`
#
# PROGRAM:
# 1. Display program name "FUNCTION IN/OUT PROGRAM".
# 2. Ask the user to enter a number. Store it in a float variable `number`.
# 3. Call the function `GetSquare`, passing in `number`. Store the result in a variable `square`.
# 4. Display "The square is:", and then the `square` variable.
# 5. At the end of the program, print "Goodbye"


## -- FUNCTION DEFINITIONS --



## -- PROGRAM MAIN --

