# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentC_p2_format.py
#
# FUNCTION 1: FormatFraction   Input: two ints: `num` and `denom`
#   Within the function, print the fraction in "num/denom" format:
#   print( str( num ) + "/" + str( denom ) )
#
# PROGRAM:
# 1. Display program name "FORMATTING PROGRAM".
# 2. Ask the user to enter a numerator. Store it as an integer in a variable `numerator`.
# 3. Ask the user to enter a denominator. Store it as an integer in a variable `denominator`.
# 4. Call the FormatFraction function, passing in the `numerator` and `denominator`.
# 5. At the end of the program, print "Goodbye"


## -- FUNCTION DEFINITIONS --



## -- PROGRAM MAIN --

