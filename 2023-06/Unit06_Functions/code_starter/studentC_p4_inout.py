# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentC_p4_inout.py
#
# FUNCTION 1: FractionToDecimal    Inputs: `num`, `denom`
#   Within the function, return `num / denom`
#
# PROGRAM:
# 1. Display program name "FUNCTION IN/OUT PROGRAM".
# 2. Ask the user to enter a numerator. Store it in a float variable `numerator`.
# 3. Ask the user to enter a denominator. Store it in a float variable `denominator`.
# 4. Call the function `FractionToDecimal`, passing in `numerator`, `denominator`.
# 5. Display "The decimal form is:", and then the `decimal` variable.
# 7. At the end of the program, print "Goodbye"


## -- FUNCTION DEFINITIONS --



## -- PROGRAM MAIN --

