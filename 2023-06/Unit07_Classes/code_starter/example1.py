# RUN PROGRAM: python3 example1.py

# -- CLASS DECLARATION -- #
class Pet:                                    # Class declaration, Pet type
  def __init__( self ):                       # Initialization function (special)
    self.name = "Cat"                         # Default Pet name to "Cat"
    self.speak = "Meow"                       # Default Pet speak to "Meow"
  # end __init__
  
  def SetName( self, new_name ):              # SetName function/method (belongs to Pet class)
    self.name = new_name                      # Set member variable name to the parameter new_name
  # end SetName
  
  def SetSpeak( self, new_speak ):            # SetSpeak function/method (belongs to Pet class)
    self.speak = new_speak                    # Set member variable speak to the parameter new_speak
  # end SetSpeak
  
  def Action( self ):                         # Action function/method (belongs to Pet class)
    print( self.name, "says", self.speak )    # Print the member variables name and speak
  # end Action
  
  def GetName( self ):
    return self.name
  # end Name
# end class


# -- PROGRAM CODE -- #

# Leaving pet1 with defaults
print( "pet1" )
pet1 = Pet()                                  # Create Pet variable
pet1.Action()                                 # Call Action function
petname = pet1.GetName()                      # Call GetName and assign it to variable `petname`.
print( "Pet name is", petname )               # Display the pet's name

# Update pet2's name and speak
print( "pet2" ) 
pet2 = Pet()                                  # Create Pet variable
pet2.SetName( "Dog" )                         # Call SetName function
pet2.SetSpeak( "Woof" )                       # Call SetSpeak function
pet2.Action()                                 # Call Action function
petname = pet2.GetName()                      # Call GetName and assign it to variable `petname`.
print( "Pet name is", petname )               # Display the pet's name

