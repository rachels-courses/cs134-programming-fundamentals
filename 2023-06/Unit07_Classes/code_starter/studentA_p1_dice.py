# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentA_p1_dice.py
#
# CLASS DECLARATION:
# Class name Die
#   Member variables: 
#     * sides (default to 6)
#
#   Member functions (methods):
#     * SetSides
#       Input parameters: `new_sides`
#       Output returns: none
#       Functionality: Assigns `self.sides` the value from `new_sides`.
#
#     * Roll
#       Input parameters: none
#       Output returns: A random number between 1 and `self.sides`

import random






# PROGRAM:
# 1. Print "Roll a d6 five times"
# 2. Create a variable `d6` that is a Die type
# 3. Use a for loop to loop 5 times. Within the loop:
#    3a. Call the d6 Die's Roll function (`d6.Roll()`) and assign it to a variable `roll`.
#    3b. Print out the result of `roll`.
#
# 4. Print "Roll a d20 five times"
# 5. Create a variable `d20` that is a Die type
# 6. Use the d20's SetSides function, passing in 20. (`d20.SetSides( 20 )`)
# 7. Use a for loop to loop 5 times. Within the loop:
#    7a. Call the d20 Die's Roll function and assign it to a variable `roll`.
#    7b. Print out the result of `roll`.






# Example output:
# Roll a d6 five times...
# Rolled: 1
# Rolled: 2
# Rolled: 2
# Rolled: 4
# Rolled: 3
# 
# Roll a d20 five times...
# Rolled: 8
# Rolled: 12
# Rolled: 15
# Rolled: 16
# Rolled: 12
