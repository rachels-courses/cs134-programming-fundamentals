# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentA_p1_dice.py

# CLASS DECLARATION:
# Class name Card
#   Member variables:
#     * suit (default to "hearts")
#     * rank (default to "A")
#
#   Member functions (methods):
#     * SetSuit
#       Input parameters: new_suit
#       Output returns: none
#       Functionality: Assigns `self.suit` the value from `new_suit`.
#
#     * SetRank
#       Input parameters: new_rank
#       Output returns: none
#       Functionality: Assigns `self.rank` the value from `new_rank`.
#
#     * Display
#       Input parameters: none
#       Output returns: none
#       Functionality: Prints the card's rank and suit.








# PROGRAM:
# 1. Print "Default card"
# 2. Create a variable `card1` that is a Card type.
# 3. Call `card1`'s Display function (`card1.Display()`)
#
# 4. Print "Changed card"
# 5. Create a variable `card2` that is a Card type.
# 6. Call `card2`'s SetSuit function, passing in "diamonds".
# 7. Call `card2`'s SetRank function, passing in "K".
# 8. Call `card2`'s Display function.







# Example output:
# Default card...
# A of hearts
# Changed card...
# K of diamonds
