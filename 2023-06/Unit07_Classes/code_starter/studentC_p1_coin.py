# STUDENT NAME: 
# SEMESTER/YEAR: 
# 
# RUN PROGRAM: python3 studentA_p1_dice.py

# CLASS DECLARATION:
# Class name Card
#   Member variables:
#     * sides, a list (default to ["heads", "tails"]
#
#   Member functions (methods):
#     * SetSides
#       Input parameters: side1, side2
#       Output returns: none
#       Functionality: Sets member variable `sides[0]` to side1 and member variable `sides[1]` to side2.
#
#     * Flip
#       Input parameters: none
#       Output returns: sides[0] or sides[1] at random.
#       Functionality: Use `random.randint( 0, 1 )` and assign it to a variable `index`. Return `self.sides[index]`.

import random






# PROGRAM:
# 1. Print "Flip heads/tails coin 5 times"
# 2. Create a variable `coin1` that is a Coin type
# 3. Use a for loop to loop 5 times. Within the loop:
#    3a. Call `coin1`'s Flip fucntion (`coin1.Flip()`) and assign it to a variable `flip`.
#    3b. Print the results of `flip`.
#
# 4. Print "Flip tacos/pizza coin 5 times"
# 5. Create a variable `coin2` that is a Coin type
# 6. Use a for loop to loop 5 times. Within the loop:
#    6a. Call `coin2`'s Flip fucntion (`coin2.Flip()`) and assign it to a variable `flip`.
#    6b. Print the results of `flip`.







# Example output:
# Flip heads/tails coin 5 times...
# Result: heads
# Result: tails
# Result: tails
# Result: heads
# Result: tails
# 
# Flip tacos/pizza coin 5 times...
# Result: tacos
# Result: tacos
# Result: pizza
# Result: tacos
# Result: pizza
