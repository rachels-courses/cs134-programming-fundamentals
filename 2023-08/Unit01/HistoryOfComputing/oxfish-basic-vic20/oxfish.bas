1 rem meant for vic-20

10 rem variables
11 ox=0
12 fish=0

20 rem program
21 print "----------------------"
22 print "main menu"
23 print "----------------------"
24 print "inventory:"
25 print "* ox:  ";ox
26 print "* fish:";fish
27 print ""
28 print "1. add stock"
29 print "2. remove stock"
30 print "3. exit"
31 print ""
32 input "choice"; menu
33 if menu=1 then 40
34 if menu=2 then 60
35 if menu=3 then 100
36 goto 20

40 rem add stock
41 print "----------------------"
42 print "add stock"
43 print "----------------------"
44 print "1. ox, 2. fish"
45 input "which"; type
46 input "how many to add"; amt
47 rem
48 rem
49 if type=1 then ox=ox+amt
50 if type=2 then fish=fish+amt
51 goto 20

60 rem remove stock
61 print "----------------------"
62 print "remove stock"
63 print "----------------------"
64 print "1. ox, 2. fish"
65 input "which"; type
66 input "how many to remove"; amt
67 rem
68 rem
69 if type=1 then ox=ox-amt
70 if type=2 then fish=fish-amt
71 goto 20

100 print "goodbye"
101 end
