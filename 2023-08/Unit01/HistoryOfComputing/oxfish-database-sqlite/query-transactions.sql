SELECT 
	T.transaction_id, T.quantity, T.type_id, 
	TYP.name, 
	O1.owner_id as "FROM", O1.name as "FROM", 
	O2.owner_id as "TO", O2.name as "TO",
	T.notes
	FROM "transaction" as T
	INNER JOIN "owner" as O1 on T.from_id=O1.owner_id
	INNER JOIN "owner" as O2 on T.to_id=O2.owner_id
	INNER JOIN "type" as TYP on T.type_id=TYP.type_id
	