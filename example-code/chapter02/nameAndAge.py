# Lines that begin with a hashtag are comments.
# These lines are ignored!


# Print a string literal
print( "Hello!" )


# Store your full name into a variable.
# Variable declaration AND assignment
#fullName = input( "What is your full name? " )
fullName = "Luna"

# Store your age in years into a variable.
age = int( input( "What is your age? " ) )

# Add 1 to the age
age = age + 1

money = input( "How much money do you have? " )

# Print values stored in variables
print( "Name:", fullName )
print( "Age: ", age )
print( "Money: $", money, sep="" )


print( "Hello \"", end="" )
print( fullName, end="" )
print( "\" how are you doing?", end="" )
