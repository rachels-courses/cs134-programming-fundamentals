# Create variables for each ingredient amount
cupsOfButter        = 1
teaspoonsVanilla    = 2

# Display ingredients
print( cupsOfButter,        " cups butter" )
print( teaspoonsVanilla,    " tsps vanilla" )

# Ask user for how many batches?
batches = float( input( "How many batches? " ) )

# Recalculate ingredient amounts based on batches
cupsOfButter        = cupsOfButter      * batches
teaspoonsVanilla    = teaspoonsVanilla  * batches

print( "\n\n UPDATED RECIPE" )

# Display updated ingredient amounts
print( cupsOfButter,        " cups butter" )
print( teaspoonsVanilla,    " tsps vanilla" )
