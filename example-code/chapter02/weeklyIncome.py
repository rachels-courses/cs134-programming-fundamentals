# Get data from user
price = float( input( "What is the cost of the item? " ) )
tax = 0.09


# Calculation
# Calculate the amount of tax added onto the price,
# add it back to the price to get price plus tax.
pricePlusTax = price + ( price * tax )


# Display result
print( "Final price: $", pricePlusTax )

